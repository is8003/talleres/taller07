# Enunciado

En un torneo hípico se registra la información de las carreras del torneo,
indicando el código del jockey ganador, el tiempo del ganador (en segundos), el
código del jockey que llegó en segundo lugar y el tiempo empleado por este (en
segundos). La información se encuentra registrada en un arreglo del estilo:

```text
Ganador | T. Ganador | Segundo | T. Segundo | Ganador | T. Ganador | Segundo | T. Segundo | ...
   3    |     120    |    1    |     124    |    2    |     185    |    3    |      191   | ...
```

Note que este arreglo tiene la información de dos carreras: mientras la primera
carrera se caracteriza por los primeros 4 datos, la segunda carrera se
caracteriza por 4 los datos siguientes. El arreglo puede contener la
información de N carreras. Además, se tiene un arreglo donde están almacenados
los nombres de los jockeys. El código del jockey es la posición que ocupa en el
arreglo.

```text
   0   |    1    |   2  |   3   |   4    |   5   |    6    | 
 JuanR | MiguelP | AnaM | LuisG | PedroJ | LinaQ | CarlosS |
```

Así, el código de JuanR es 0, el código de MiguelP es 1 y así sucesivamente.

Teniendo en cuenta la información disponible, lo que se solicita es:

1. Elabore una función que retorne el nombre del jockey que ganó más carreras
   en el torneo. Tenga cuidado al definir los parámetros.

2. Elabore una función que genere un arreglo nuevo donde se consigne la
   puntuación que se da a los jockeys participantes. Asegúrese que el nuevo
   arreglo esté disponible para las demás funciones. Tenga en cuenta que algún
   jockey podría no tener puntos. El esquema de puntuación es el siguiente: 

   1. Por cada carrera ganada se le otorgan 5 puntos. Si la diferencia de
   tiempo entre el ganador y el segundo puesto es mayor a 5 segundos se le
   otorga un punto adicional al ganador. 

   1. Por cada vez que ocupa el segundo puesto se le otorgan 3 puntos.

3. Elabore una función que produzca el reporte del torneo. El reporte debe
   incluir el nombre del jockey y los puntos obtenidos, ordenado de manera
   descendente teniendo como criterio los puntos.

## Refinamiento 1

### Función: `ganador`

Retorna nombre del jinete que ganó más carreras.

#### Entradas

- Arreglo con resultado de carreras.
- Tamaño del arreglo con resultado de carreras.
- Arreglo con nombres de jinetes.

#### Salida

- Nombre del jinete ganador. 

#### Pseudo-código

1. Creo arreglo de victorias con tamaño igual a cantidad de jinetes e
   inicializado en ceros. 
1. Recorro arreglo con resultado de carreras cada cuatro posiciones:
   1. Sumo victoria a la posición equivalente al identificador de jinete.
1. Creo variable para almacenar máxima cantidad de victorias de un jinete e
   inicializo a `0`.
1. Creo variable para almacenar identificador de jinete ganador.
1. Recorro arreglo con victorias:
   1. Si cantidad de victorias del jinete es mayor a máxima cantidad de
      victorias:
      1. Asigno cantidad de victorias del jinete a máxima cantidad de
	 victorias.
      1. Asigno posición de elemento actual al identificador de jinete ganador.
1. Retorno nombre con identificador de jinete ganador.

### Función: `calcularPuntos`

Calcula puntaje por jinete a partir de resultado de carreras.

#### Entradas

- Arreglo con resultado de carreras.
- Tamaño del arreglo con resultado de carreras.
- Arreglo para almacenar puntaje por jinete.
- Tamaño de arreglo para almacenar puntaje por jinete.

#### Salidas

- Ninguna.

#### Pseudo-código

1. Recorro arreglo de puntajes para inicializar todos los elementos a cero.
1. Recorro arreglo de resultado de carreras, y cada cuatro elementos:
   1. Incremento `5` puntos a elemento de jinete con identificador de ganador.
   1. Si diferencia entre tiempo de segundo y primer jinete es mayor a `5`:
      1. Incremento `1` punto a elemento de jinete con identificador de
	 ganador.
   1. Incremento `3` puntos a elemento de jinete con identificador de segundo
      puesto.

### Función: `reportarTorneo`

Imprime en pantalla tabla descendente de posiciones con nombres de jinetes y
puntos obtenidos durante torneo.

#### Entradas
- Arreglo con nombres de jinetes.
- Arreglo con puntaje por jinete.
- Cantidad de jinetes (tamaño de arreglos).

#### Salidas

- Ninguna.

#### Pseudo-código

1. Creo arreglo para almacenar identificadores de jinetes.
1. Recorro arreglo de identificadores de jinetes y asigno por posición cada
   identificador (secuencia ascendente).
1. Recorro arreglo de puntajes desde posición cero hasta penúltimo:
   1. Creo variable para almacenar posición de máximo y asigno posición actual.
   1. Recorro arreglo de puntajes desde posición actual hasta el último:
      1. Selecciono el puntaje más grande del sub-arreglo y su posición.
   1. Si encuentro puntaje más grande con respecto a posición actual:
      1. Intercambio puntaje más grande con posición actual en arreglo de
	 puntajes. 
      1. Intercambio identificador de jinete con puntaje más grande con
	 posición actual en arreglo de identificadores de jinetes. 
1. Imprimo tabla de posiciones del torneo con nombres y puntajes.

## Refinamiento 2

### Función: `ganador`

Retorna nombre del jinete que ganó más carreras.

#### Entradas

- `reCarr`: arreglo con resultado de carreras.
- `n`: tamaño del arreglo con resultado de carreras.
- `nom`: arreglo con nombres de jinetes.

#### Salida

- Nombre del jinete ganador. 

#### Pseudo-código

1. Declaro arreglo de `victorias` con tamaño igual a cantidad de jinetes e
   inicializado en ceros. 
1. Para `i` igual a `0`; hasta `i` menor que tamaño `n` de arreglo `reCarr`;
   incremento `i` en `4`:
   1. Sumo 1 a `victorias` en posición `reCarr[i]`.
1. Declaro variable `maxVic` para almacenar máxima cantidad de victorias de un
   jinete e inicializo a `0`.
1. Declaro variable `idGan` para almacenar identificador de jinete ganador.
1. Para  `i` igual a 0; hasta `i` menor que cantidad de jinetes;
   incremento `i` en `1`:
   1. Si cantidad de victorias del jinete `victorias[i]` es mayor a máxima
      cantidad de victorias `maxVic`:
      1. Asigno cantidad de victorias del jinete `victorias[i]` a máxima
	 cantidad de victorias `maxVic`.
      1. Asigno posición de elemento actual `i` al identificador de jinete
	 ganador `idGan`.
1. Retorno nombre con identificador de jinete ganador `nom[idGan]`.

### Función: `calcularPuntos`

Calcula puntaje por jinete a partir de resultado de carreras.

#### Entradas

- `reCarr`: Arreglo con resultado de carreras.
- `nCarr`: Tamaño del arreglo con resultado de carreras.
- `punt`: Arreglo para almacenar puntaje por jinete.
- `nPunt`: Tamaño de arreglo para almacenar puntaje por jinete.

#### Salidas

- Ninguna.

#### Pseudo-código

1. Para `i` desde `0`; hasta `i` menor a tamaño de arreglo de puntajes `nPunt`;
   incremento `i` en `1`: 
   1. Asigno a elemento `punt[i]` valor `0`.
1. Para `i` desde `0`; hasta `i` menor a tamaño de arreglo de resultados
   `nCarr`; incremento `i` en `4`:
   1. Incremento `5` puntos a elemento de jinete con identificador de ganador
      `punt[reCarr[i]]`.
   1. Si diferencia entre tiempo de segundo `reCarr[i + 3]` y primer jinete
      `reCarr[i + 1]` es mayor a `5`:
      1. Incremento `1` punto a elemento de jinete con identificador de
	 ganador `punt[reCarr[i]]`.
   1. Incremento `3` puntos a elemento de jinete con identificador de segundo
      puesto `punt[reCarr[i + 2]]`.

### Función: `reportarTorneo`

Imprime en pantalla tabla descendente de posiciones con nombres de jinetes y
puntos obtenidos durante torneo.

#### Entradas
- `nom`: arreglo con nombres de jinetes.
- `punt`: arreglo con puntaje por jinete.
- `jin`: cantidad de jinetes (tamaño de arreglos).

#### Salidas

- Ninguna.

#### Pseudo-código

1. Declaro arreglo para almacenar identificadores de jinetes `idJin`.
1. Para `i` desde `0`; hasta `i` menor a `jin`; incremento `i` en `1`:
   1. Asigno valor de `i` como identificador de jinete `idJin[i]`
1. Para `i` desde `0`; hasta `i` menor a `jin` menos `1`; incremento `i` en
   `1`:
   1. Creo variable para almacenar posición de máximo `posMax` y asigno
      posición actual `i`.
   1. Para `j` desde `i + 1`; hasta `j` menor a `jin`; incremento `j` en `1`:
      1. Si puntaje en posición `j` es major a puntaje en posición `maxPos`:
	 1. Asigno a posición de maximo encontrado `posMax` el valor de `j`.
   1. Si posición de máximo encontrado `maxPos` es diferente a posición actual
      `i`:
      1. Creo variable para almacenar temporalmente puntaje `auxPunt` y asigno
	 valor de puntaje actual `punt[i]`.
      1. Asigno a posición de puntaje actual `punt[i]` el puntaje máximo
	 encontrado `punt[posMax]`. 
      1. Asigno el valor temporal de `auxPunt` a la posición donde se
	 encontraba el valor máximo `punt[posMax]`.
      1. Creo variable para almacenar temporalmente identificador `auxId` y
	 asigno valor de identificador actual `idJin[i]`.
      1. Asigno a posición de identificador actual `idJin[i]` el identificador
	 máximo encontrado `idJin[posMax]`. 
      1. Asigno el valor temporal de `auxId` a la posición donde se encontraba
	 el valor máximo `idJin[posMax]`.
1. Imprimo encabezado de tabla de posiciones del torneo con nombres y puntajes.
1. Para `i` desde `0`; hasta `i` menor a `jin`; incremento `i` en `1`:
   1. Imprimo en una línea posición `i`, nombre de jinete `nom[idJin[i]]` y
      puntaje `punts[i]`.
