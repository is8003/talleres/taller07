#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define JINETES 7
#define CARRERAS 10

void generarCarreras(unsigned reCarr[], unsigned n, unsigned jin);
void imprimirCarreras(unsigned reCarr[], unsigned n, const char *nom[]);
const char * ganador(unsigned reCarr[], unsigned n, const char *nom[]);
void calcularPuntos(unsigned reCarr[], unsigned nCarr, unsigned punt[],
		unsigned nPunt);
void reportarTorneo(const char * nom[], unsigned punt[], unsigned jin);

int main(void){
	unsigned resulCarre[CARRERAS * 4];
	const char * nombres[JINETES] = { "JuanR", "MiguelP", "AnaM", "LuisG",
		"PedroJ", "LinaQ", "CarlosS"};
	unsigned puntos[JINETES];

	srand(time(NULL));

	generarCarreras(resulCarre, CARRERAS * 4, JINETES);
	imprimirCarreras(resulCarre, CARRERAS * 4, nombres);

	printf("\nJinete con más victorias: %s\n\n",
			ganador(resulCarre, CARRERAS * 4, nombres));

	calcularPuntos(resulCarre, CARRERAS * 4, puntos, JINETES);

	reportarTorneo(nombres, puntos, JINETES);
}


void generarCarreras(unsigned reCarr[], unsigned n, unsigned jin){
	for (unsigned i = 0; i < n; i += 4){
		reCarr[i] = rand() % jin;
		reCarr[i + 1] = (rand() % 100) + 100;

		do{
			reCarr[i+2] = rand() % jin;
		}while(reCarr[i] == reCarr[i + 2]);

		do{
			reCarr[i + 3] = (rand() % 100) + 101;
		}while(reCarr[i+1] >= reCarr[i + 3]);
	}
}

void imprimirCarreras(unsigned reCarr[], unsigned n, const char *nom[]){
	puts("            |        1ro           |        2do           |");
	puts("Carrera No. | Nombre  | Tiempo [s] | Nombre  | Tiempo [s] |");
	for (unsigned i = 0; i < n; i += 4){
		printf("%11d |", i/4 + 1);
		printf("%8s |", nom[reCarr[i]]);
		printf("%11u |", reCarr[i + 1]);
		printf("%8s |", nom[reCarr[i+2]]);
		printf("%11u |\n", reCarr[i + 3]);
	}
}

/* ### Función: ganador
 * Retorna nombre del jinete que ganó más carreras.
 * #### Entradas
 * - reCarr: arreglo con resultado de carreras.
 * - n: tamaño del arreglo con resultado de carreras.
 * - nom: arreglo con nombres de jinetes.
 * #### Salida
 * - Nombre del jinete ganador. */
const char * ganador(unsigned reCarr[], unsigned n, const char *nom[]){
	unsigned victorias[JINETES] = {0};

	for (unsigned i = 0; i < n; i += 4){
		victorias[reCarr[i]]++;
	}

	unsigned maxVic = 0;
	unsigned idGan;

	for (unsigned i = 0; i < JINETES; i++){
		if (victorias[i] > maxVic){
			maxVic = victorias[i];
			idGan = i;
		}
	}
	return nom[idGan];
}


/* ### Función: calcularPuntos
 * Calcula puntaje por jinete a partir de resultado de carreras.
 * #### Entradas
 * - reCarr: Arreglo con resultado de carreras.
 * - nCarr: Tamaño del arreglo con resultado de carreras.
 * - punt: Arreglo para almacenar puntaje por jinete.
 * - nPunt: Tamaño de arreglo para almacenar puntaje por jinete.
 * #### Salidas
 * - Ninguna. */
void calcularPuntos(unsigned reCarr[], unsigned nCarr, unsigned punt[],
		unsigned nPunt){
	for (unsigned i = 0; i < nPunt; i++){
		punt[i] = 0;
	}

	for (unsigned i = 0; i < nCarr; i +=4 ){

		punt[reCarr[i]] += 5;

		if (reCarr[i + 3] - reCarr[i + 1] > 5){
			punt[reCarr[i]]++;
		}

		punt[reCarr[i + 2]] += 3;
	}
}


/* ### Función: reportarTorneo
 * Imprime en pantalla tabla descendente de posiciones con nombres de jinetes y
 * puntos obtenidos durante torneo.
 * #### Entradas
 * - nom: arreglo con nombres de jinetes.
 * - punt: arreglo con puntaje por jinete.
 * - jin: cantidad de jinetes (tamaño de arreglos).
 * #### Salidas
 * - Ninguna. */
void reportarTorneo(const char * nom[], unsigned punt[], unsigned jin){
	unsigned idJin[JINETES];

	for (unsigned i = 0; i < jin; i++){
		idJin[i] = i;
	}

	for (unsigned i = 0; i < jin - 1; i++){
		unsigned posMax = i;
		for (unsigned j = i + 1; j < jin; j++){
			if (punt[j] > punt[posMax]){
				posMax = j;
			}
		}

		if ( posMax != i){
			unsigned auxPunt = punt[i];
			punt[i] = punt[posMax];
			punt[posMax] = auxPunt;

			unsigned auxId = idJin[i];
			idJin[i] = idJin[posMax];
			idJin[posMax] = auxId;

		}
	}

	puts("Pos. |  Nombre | Puntos |");
	for (unsigned i = 0; i < jin; i++){
		printf("%4u | %7s | %6u |\n", i+1, nom[idJin[i]], punt[i]);
	}
}
